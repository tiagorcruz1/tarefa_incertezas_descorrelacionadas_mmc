# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 21:06:00 2019

@author: tiago
"""

import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

# numero de iteracoes
M = 1000000

# matriz de correlacao
corr = np.zeros((5,5)) + 0.9 # alta correlação
np.fill_diagonal(corr,1) # diagonal é 1

# cria matriz L por Choesky
L = np.linalg.cholesky(corr)

# distribuicoes de entrada
x_1 = np.random.normal(0,1,M)
x_2 = np.random.normal(0,1,M)
x_3 = np.random.normal(0,1,M)
x_4 = np.random.normal(0,1,M)
x_5 = np.random.normal(0,1,M)

X = [x_1, x_2, x_3, x_4, x_5]

# valores medios dados no problema
media_x_1 = 10
media_x_2 = 20
media_x_3 = 10
media_x_4 = 20
media_x_5 = 10

# incertezas padrao dadas no problema
u_1 = 0.077/2
u_2 = 0.084/2
u_3 = 0.075/2
u_4 = 0.079/2
u_5 = 0.070/2

# ajuste para os valores de interesse na distribuicao de entrada
medidas_1 = x_1*u_1+media_x_1
medidas_2 = x_2*u_2+media_x_2
medidas_3 = x_3*u_3+media_x_3
medidas_4 = x_4*u_4+media_x_4
medidas_5 = x_5*u_5+media_x_5

# distribuicoes de saida
y = medidas_1 + medidas_2 + medidas_3 + medidas_4 + medidas_5

# media de saida
media_saida = np.mean(y)

# incerteza pára 1 desvio padrao
corte = int((1-0.6826)/2*M)
y = np.sort(y)
y_1stdev = y[corte:M-corte]
# incerteza pára 2 desvios padrao
corte_2 = int((1-0.954)/2*M)
y = np.sort(y)
y_2stdev = y[corte_2:M-corte_2]
# incerteza para 1 desvio padrao
U_a = (y_1stdev[len(y_1stdev)-1] - y_1stdev[0])/2
U_b = (y_2stdev[len(y_2stdev)-1] - y_2stdev[0])/2

plt.figure(1)
plt.hist(y,200)
plt.title('Distribuição da saída [y]')