# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 15:15:31 2019

@author: UFRGS
"""

import numpy as np
import matplotlib.pyplot as plt
from statistics import stdev
from math import sqrt

plt.close('all')

M = 1000000

# incerteza de medidas
x = [160.23, 160.18, 160.17, 160.18]
media_x = np.mean(x)
i_x = stdev(x)/sqrt(len(x)) # stdev - desvio padrao

# distribuicoes de entrada
dist_x = i_x*np.random.standard_t(len(x)-1, M) + media_x
dist_calibrador_voltimetro = np.random.uniform(-0.05,0.05, M) # resolucao = 0.1/2
dist_calibrador_calibrador = np.random.uniform(-0.005,0.005, M) # resolucao = 0.01/2
dist_calibrador = np.random.normal(0,0.0005, M) # #(media, desv_pad, size)
dist_drift = np.random.uniform(-0.002, 0.002, M)

# saida
y = dist_x + dist_calibrador_voltimetro + dist_calibrador_calibrador + dist_calibrador + dist_drift
# incerteza com 100% de confiaça
#U_100 = (max(y) - min(y))/2

# media
y_media = np.mean(y)

# 
corte = int(0.05/2*M)
y = np.sort(y)
y_95 = y[corte:M-corte]
# incerteza para 95%
U = (y_95[len(y_95)-1] - y_95[0])/2 #ponto final - inicial /2
print("U = ",U)

plt.figure(1)
plt.subplot(3,2,1)
plt.hist(dist_x,200) # (data, faixas)
plt.title("Medidas")
plt.subplot(3,2,2)
plt.hist(dist_calibrador_voltimetro,200)
plt.title("Estabilidade temporal")
plt.subplot(3,2,3)
plt.hist(dist_calibrador_calibrador,200)
plt.title("Estabilidade térmica")
plt.subplot(3,2,4)
plt.hist(dist_calibrador,200)
plt.title("Resolução")
plt.subplot(3,2,5)
plt.hist(dist_drift,200)
plt.title("Tabela de calibração")
plt.subplot(3,2,6)
plt.hist(y,200, color='black')
plt.title("Histograma da saída")