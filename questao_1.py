# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 12:15:09 2019

@author: UFRGS
"""

import numpy as np
import matplotlib.pyplot as plt
from statistics import stdev
from math import sqrt

plt.close('all')

M = 1000000

# incerteza de medidas
x = [19.85, 20, 20.05, 19.95, 20, 19.85, 19.9, 20.05, 19.95, 19.85, 19.9, 20.05]
media_x = np.mean(x)
i_x = stdev(x)/sqrt(len(x)) # stdev - desvio padrao

# distribuicoes de entrada
dist_x = i_x*np.random.standard_t(len(x)-1, M) + media_x
dist_tempo = np.random.uniform(-0.1,0.1, M) # estabilidade temporal = 0.1
dist_termico = np.random.uniform(-0.025,0.025, M) # estabilidade termica = 0.025
dist_resolucao = np.random.uniform(-0.025,0.025, M) #resolucao = 0.05/2
dist_tabela = np.random.normal(0, 0.04, M) #(media, desv_pad, size)

# erros sistematicos
erro_termico = 0.125
erro_calibracao = 0.15

# saida
y = dist_x + dist_tempo + dist_termico + dist_resolucao + dist_tabela - erro_termico - erro_calibracao
# incerteza com 100% de confiaça
#U_100 = (max(y) - min(y))/2

# media
y_media = np.mean(y)

# 
corte = int(0.05/2*M)
y = np.sort(y)
y_95 = y[corte:M-corte]
# incerteza para 95%
U = (y_95[len(y_95)-1] - y_95[0])/2 #ponto final - inicial /2
print("U = ",U)

plt.figure(1)
plt.subplot(3,2,1)
plt.hist(dist_x,200) # (data, faixas)
plt.title("Medidas")
plt.subplot(3,2,2)
plt.hist(dist_tempo,200)
plt.title("Estabilidade temporal")
plt.subplot(3,2,3)
plt.hist(dist_termico,200)
plt.title("Estabilidade térmica")
plt.subplot(3,2,4)
plt.hist(dist_resolucao,200)
plt.title("Resolução")
plt.subplot(3,2,5)
plt.hist(dist_tabela,200)
plt.title("Tabela de calibração")
plt.subplot(3,2,6)
plt.hist(y,200, color='black')
plt.title("Histograma da saída")