# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 21:56:43 2019

@author: tiago
"""

import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

# numero de iteracoes
M = 100000

# matriz de correlacao
corr = np.zeros((3,3))
np.fill_diagonal(corr,1) # diagonal é 1
corr[0,1] = corr[1,0] = 0.5
corr[0,2] = corr[2,0] = -0.5
corr[1,2] = corr[2,1] = 0.05

# cria matriz L por Choesky
L = np.linalg.cholesky(corr)

# distribuicoes de entrada uniformes com media zero
x_1 = np.random.uniform(-1,1,M)
x_2 = np.random.uniform(-1,1,M)
x_3 = np.random.uniform(-1,1,M)

# distribuicoes de saida
y = x_1 + x_2 + x_3

# media de saida
media_saida = np.mean(y)

# incerteza para 95% confianca
corte = int(0.05/2*M)
y = np.sort(y)
y_95 = y[corte:M-corte] # armazena valores de interesse (95%)
U = (y_95[len(y_95)-1] - y_95[0])/2

#plots
plt.figure(1)
plt.plot(x_1,x_2,',')
plt.title('Correlação entre x_1 e x_2 = 0.5')
plt.xlabel('x_1'), plt.ylabel('y_1')
plt.figure(2)
plt.plot(x_2,x_3,',')
plt.title('Correlação entre x_1 e x_3 = -0.5')
plt.xlabel('x_2'), plt.ylabel('y_2')
plt.figure(3)
plt.plot(x_1,x_3,',')
plt.title('Correlação entre x_2 e x_3 = 0.05')
plt.xlabel('x_3'), plt.ylabel('y_3')