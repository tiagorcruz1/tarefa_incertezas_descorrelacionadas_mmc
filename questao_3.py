# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 16:40:48 2019

@author: UFRGS
"""
import matplotlib.pyplot as plt
import numpy as np
from statistics import stdev

plt.close('all')

# numero de iteracoes
M = 1000000

# coeficientes de correlacao dados no exemplo
corr_v_i = -0.36
corr_v_phi = 0.86
corr_i_phi = -0.65

# matriz de correlacao
corr = np.zeros((3,3))
np.fill_diagonal(corr,1) # diagonal é 1
corr[0,1] = corr[1,0] = corr_v_i
corr[0,2] = corr[2,0] = corr_v_phi
corr[1,2] = corr[2,1] = corr_i_phi

# cria matriz L por Cholesky
L = np.linalg.cholesky(corr)

# distribuicoes de entrada 
x_0 = np.random.normal(0,1,M)
x_1 = np.random.normal(0,1,M)
x_2 = np.random.normal(0,1,M)

# cria matriz X
x = [x_0, x_1, x_2]

# cria matriz Y com entrdas correlacionadas (Y = LX)
x_corr = L@x
#np.corrcoef(x_corr) # teste 

# valores medios dados no exemplo
v_medio = 4.999
i_medio = 19.661e-3
phi_medio = 1.04446

# desvio padrao = incerteza padrao
s_v = 0.0032
s_i = 0.0095e-3
s_phi = 0.00075

# ajuste para os valores de interesse
v = x_corr[0]*s_v+v_medio
i = x_corr[1]*s_i+i_medio
phi = x_corr[2]*s_phi+phi_medio

# distribuicoes de saida
R = (v/i)*np.cos(phi)
X = (v/i)*np.sin(phi)
Z = (v/i)

# medias
R_medio = np.mean(R)
X_medio = np.mean(X)
Z_medio = np.mean(Z)

# incerteza pára 1 desvio padrao
corte = int((1-0.6826)/2*M)
R = np.sort(R)
X = np.sort(X)
Z = np.sort(Z)
R_1stdev = R[corte:M-corte]
X_1stdev = X[corte:M-corte]
Z_1stdev = Z[corte:M-corte]
# incerteza para 1 desvio padrao
U_r = (R_1stdev[len(R_1stdev)-1] - R_1stdev[0])/2 #ponto final - inicial /2
U_x = (X_1stdev[len(X_1stdev)-1] - X_1stdev[0])/2
U_z = (Z_1stdev[len(Z_1stdev)-1] - Z_1stdev[0])/2

plt.figure(1)
plt.subplot(3,1,1)
plt.title('Resistência [R]')
plt.hist(R,200)
plt.subplot(3,1,2)
plt.title('Reatância [X]')
plt.hist(X, 200)
plt.subplot(3,1,3)
plt.title('Impedância [Z]')
plt.hist(Z, 200)
